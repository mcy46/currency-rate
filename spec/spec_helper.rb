require "uri"
require "yaml"
require "webmock/rspec"
require_relative "../lib/currency_rate"

Dir[File.join CurrencyRate.root, "spec/support/**/*.rb"].each { |f| require f }

def adapter_class(name)
  @adapter_classes ||= {}
  @adapter_classes[name] ||= CurrencyRate.const_get(name.to_camel_case + "Adapter")
end

def adapter_instance(name)
  @adapter_instances ||= {}
  return @adapter_instances[name] if @adapter_instances[name]
  container_mock = double("CurrencyRate::Container mock")
  allow(container_mock).to receive(:log)
  allow(container_mock).to receive(:connect_timeout).and_return(10)
  allow(container_mock).to receive(:read_timeout).and_return(10)
  adapter_class(name).instance.container = container_mock
  adapter_class(name).instance.api_key = @api_keys[name.to_snake_case]
  @adapter_instances[name] = adapter_class(name).instance
end

def fetch_urls_for(adapter_name)
  @fetch_urls ||= {}
  return @fetch_urls[adapter_name] if @fetch_urls[adapter_name]
  urls = adapter_class(adapter_name)::FETCH_URL
  urls = urls.kind_of?(String) ? { adapter_name => urls } : urls
  @fetch_urls[adapter_name] = urls
end

def download_missing_rates_for_stubs!(name)
  print "ATTENTION: no stub file found for '#{name}' exhange,\n"
  print "will now try to connect and save the results as raw data and normalized data... "
  urls = fetch_urls_for(name)
  WebMock.disable!
  resp = adapter_instance(name).exchange_data
  WebMock.enable!
  File.binwrite "#{__dir__}/fixtures/adapters/raw_data/#{name}.data", Marshal.dump(resp)
  File.write "#{__dir__}/fixtures/adapters/normalized_data/#{name}.yml", adapter_instance(name).normalize(resp).to_yaml
  puts "done"
end

def raw_data_for(name)
  raw_data_fn = "#{__dir__}/fixtures/adapters/raw_data/#{name}.data"
  resp = File.exists?(raw_data_fn) ? Marshal.load(File.binread(raw_data_fn)) : nil
end

def normalized_data_for(name)
  floating = YAML.load_file("#{__dir__}/fixtures/adapters/normalized_data/#{name}.yml")
  floating.each { |k, v| floating[k] = BigDecimal(v.to_s) if k != "anchor" }
end

def data_for(name)
  fetch_urls_for(name).each_value do |u|
    stub_request(:any, /#{URI.parse(u).host.gsub('.', '\.')}/).to_return(body: raw_data_for(name).to_json, status: 200)
  end
  unless File.exists?("#{__dir__}/fixtures/adapters/raw_data/#{name}.data")
    download_missing_rates_for_stubs!(name)
  end
  [raw_data_for(name), normalized_data_for(name)]
end

RSpec.configure do |config|
  config.default_formatter = "doc"
  config.before(:all) do
    @api_keys = YAML.load_file("#{__dir__}/api_keys.yml")
  end
end

CurrencyRate.update_default_config({
  logger_settings:  { level: :info, device: "/dev/null" },
  storage_settings: { type: :file, path: File.expand_path("fixtures/adapters/normalized_data", __dir__) }
})
