require "spec_helper"

RSpec.describe CurrencyRate::HitBTCAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :hitBTC
    expect(adapter_instance("HitBTC").normalize(data)).to eq(normalized)
  end

end
