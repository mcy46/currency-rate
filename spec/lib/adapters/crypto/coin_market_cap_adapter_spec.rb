require "spec_helper"

RSpec.describe CurrencyRate::CoinMarketCapAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :coin_market_cap
    expect(adapter_instance("CoinMarketCap").normalize(data)).to eq(normalized)
  end

end
