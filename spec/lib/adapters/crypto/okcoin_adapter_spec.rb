require "spec_helper"

RSpec.describe CurrencyRate::OkcoinAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :okcoin
    expect(adapter_instance("Okcoin").normalize(data)).to eq(normalized)
  end

end
