require "spec_helper"

RSpec.describe CurrencyRate::LocalbitcoinsAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :localbitcoins
    expect(adapter_instance("Localbitcoins").normalize(data)).to eq(normalized)
  end

end
