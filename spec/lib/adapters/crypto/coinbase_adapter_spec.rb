require "spec_helper"

RSpec.describe CurrencyRate::CoinbaseAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :coinbase
    expect(adapter_instance("Coinbase").normalize(data)).to eq(normalized)
  end

end
