require "spec_helper"

RSpec.describe CurrencyRate::BitstampAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :bitstamp
    expect(adapter_instance("Bitstamp").normalize(data)).to eq(normalized)
  end

end
