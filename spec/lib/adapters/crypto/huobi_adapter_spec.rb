require "spec_helper"

RSpec.describe CurrencyRate::HuobiAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :huobi
    expect(adapter_instance("Huobi").normalize(data)).to eq(normalized)
  end

end
