require "spec_helper"

RSpec.describe CurrencyRate::BitpayAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :bitpay
    expect(adapter_instance("Bitpay").normalize(data)).to eq(normalized)
  end

end
