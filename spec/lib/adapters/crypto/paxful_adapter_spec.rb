require "spec_helper"

RSpec.describe CurrencyRate::PaxfulAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :paxful
    expect(adapter_instance("Paxful").normalize(data)).to eq(normalized)
  end

end
