require "spec_helper"

RSpec.describe CurrencyRate::BinanceAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :binance
    expect(adapter_instance("Binance").normalize(data)).to eq(normalized)
  end

end
