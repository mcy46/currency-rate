require "spec_helper"

RSpec.describe CurrencyRate::ExmoAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :exmo
    expect(adapter_instance("Exmo").normalize(data)).to eq(normalized)
  end

end
