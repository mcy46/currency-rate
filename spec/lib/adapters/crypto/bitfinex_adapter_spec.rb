require "spec_helper"

RSpec.describe CurrencyRate::BitfinexAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :bitfinex
    expect(adapter_instance("Bitfinex").normalize(data)).to eq(normalized)
  end

end
