require "spec_helper"

RSpec.describe CurrencyRate::YadioAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :yadio
    expect(adapter_instance("Yadio").normalize(data)).to eq(normalized)
  end

end
