require "spec_helper"

RSpec.describe CurrencyRate::PoloniexAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :poloniex
    expect(adapter_instance("Poloniex").normalize(data)).to eq(normalized)
  end

end
