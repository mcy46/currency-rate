require "spec_helper"

RSpec.describe CurrencyRate::KrakenAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :kraken
    expect(adapter_instance("Kraken").normalize(data)).to eq(normalized)
  end

end
