require "spec_helper"

RSpec.describe CurrencyRate::ForgeAdapter do

  before(:each) do
    @data, @normalized = data_for :forge
    @adapter = adapter_instance("Forge")
  end

  it "normalizes data to canonical form" do
    expect(@adapter.normalize(@data)).to eq(@normalized)
  end

  it "returns nil if API key is not provided" do
    @adapter.api_key = nil
    expect(@adapter.exchange_data).to eq(nil)
    @adapter.api_key = @api_keys["forge"]
    expect(@adapter.exchange_data).not_to be_nil
  end

end
