require "spec_helper"

RSpec.describe CurrencyRate::BonbastAdapter do

  it "normalizes data to canonical form" do
    raw, normalized = data_for :bonbast
    expect(adapter_instance("Bonbast").normalize(raw)).to eq(normalized)
  end

end
