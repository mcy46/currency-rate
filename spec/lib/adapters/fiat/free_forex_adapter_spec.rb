require "spec_helper"

# Because freeforexapi.com doesn't normally work and you need to place
# a small banner on your website so they'd list your websites IP address
# the API requests are stubbed (as all others) they are also excluded from forced
# updates and checks.

RSpec.describe CurrencyRate::FreeForexAdapter do

  it "normalizes data to canonical form" do
    data, normalized = data_for :free_forex
    expect(adapter_instance("FreeForex").normalize(data)).to eq(normalized)
  end

end
