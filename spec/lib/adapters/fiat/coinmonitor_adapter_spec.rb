require "spec_helper"

RSpec.describe CurrencyRate::CoinmonitorAdapter do

  it "normalizes data to canonical form" do
    raw, normalized = data_for :coinmonitor
    expect(adapter_instance("Coinmonitor").normalize(raw)).to eq(normalized)
  end

end
