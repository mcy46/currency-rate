require "spec_helper"

RSpec.describe CurrencyRate::CurrencyLayerAdapter do

  before(:each) do
    @data, @normalized = data_for :currency_layer
    @adapter = adapter_instance("CurrencyLayer")
  end

  it "normalizes data to canonical form" do
    expect(@adapter.normalize(@data)).to eq(@normalized)
  end

  it "returns nil when API key is not defined" do
    @adapter.api_key = nil
    expect(@adapter.exchange_data).to eq(nil)
    @adapter.api_key = @api_keys["currency_layer"]
    expect(@adapter.exchange_data).not_to be_nil
  end

end
