require 'spec_helper'

EXCHANGE_STUB_DOMAIN = "http://exchange.stub.com"

RESPONSE = {
  "btc_usd" => 5000,
  "ltc_usd" => 50,
}

class CurrencyRate::HashUrlAdapter < CurrencyRate::Adapter
  FETCH_URL = {
    "btc_usd" => "#{EXCHANGE_STUB_DOMAIN}/btc_usd",
    "ltc_usd" => "#{EXCHANGE_STUB_DOMAIN}/ltc_usd",
  }
end

class CurrencyRate::StringUrlAdapter < CurrencyRate::Adapter
  FETCH_URL = EXCHANGE_STUB_DOMAIN
end

RSpec.describe CurrencyRate::Adapter do

  it "raises an error when FETCH_URL is not defined" do
    adapter = CurrencyRate::Adapter.instance
    expect { adapter.exchange_data }.to raise_error("FETCH_URL is not defined!")
  end

  it "fetches data for each defined pair when FETCH_URL is a Hash" do
    adapter = adapter_instance("HashUrl")
    response_map = Hash[RESPONSE.map { |k, v| [k, { "price" => v } ] }]
    RESPONSE.each { |k,v| stub_request(:get, "#{EXCHANGE_STUB_DOMAIN}/#{k}").to_return(body: "{ \"price\": #{v} }")}
    expect(adapter.parse_raw_data(adapter.exchange_data)).to eq(response_map)
  end

  it "fetches all data from exchange and retruns it as a non-parsed json string when FETCH_URL is String" do
    adapter = adapter_instance("StringUrl")
    stub_request(:get, EXCHANGE_STUB_DOMAIN).to_return(body: RESPONSE.to_json)
    expect(adapter.exchange_data).to eq(RESPONSE.to_json)
  end

end
