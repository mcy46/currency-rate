require 'spec_helper'

RSpec.describe CurrencyRate::Container do

  if ENV["CHECK_REQUESTS"]
    it "check all adapters https request are functioning correctly and fetching the expected data" do
      WebMock.disable!
      container = CurrencyRate::Container.new(api_keys: @api_keys, logger_settings: { device: $stdout, level: :warn })
      container.adapters.delete_if { |a| a.name == "free_forex" }
      allow(container.storage).to receive(:write)
      print "Starting API checks for all adapters, might take a while..."
      successful, failed = container.sync!
      print "done"
      expect(failed).to be_empty
      container.adapters.each do |a|
        a.rates.each do |k,v|
          expect(v).to be_kind_of(BigDecimal) unless k == "anchor"
        end
      end
      WebMock.enable!
    end
  end

  describe "loading adapters on initialization" do

    it "loads all adapters, unless type or name specified" do
      container = CurrencyRate::Container.new
      expect(container.adapters).to include(*[CurrencyRate::BitstampAdapter, CurrencyRate::CurrencyLayerAdapter])
    end

    it "loads only crypto adapters" do
      container = CurrencyRate::Container.new(adapter_type: :crypto)
      expect(container.adapters).to     include(CurrencyRate::BitstampAdapter)
      expect(container.adapters).not_to include(CurrencyRate::CurrencyLayerAdapter)
    end

    it "loads only fiat adapters" do
      container = CurrencyRate::Container.new(adapter_type: :fiat)
      expect(container.adapters).not_to include(CurrencyRate::BitstampAdapter)
      expect(container.adapters).to     include(CurrencyRate::CurrencyLayerAdapter)
    end

    it "loads only adapters which names were specified" do
      container = CurrencyRate::Container.new(adapter_names: ["Bitstamp", "CurrencyLayer"])
      expect(container.adapters).to match_array([CurrencyRate::BitstampAdapter, CurrencyRate::CurrencyLayerAdapter])
    end

  end

  describe "logging" do

    before(:each) do
      @callback_called = false
      @container = CurrencyRate::Container.new(
        logger_callbacks: { warn: -> (level, messsage) { @callback_called = true }}
      )
    end

    it "converts every key in logger_callbacks Hash into a Logger::Severity constant" do
      expect(@container.logger_callbacks.keys).to include(Logger::Severity::WARN)
    end

    it "calls a logger callback if there is one on the the severity equal or lower to the current log message" do
      @container.log(:info, "Some info about whatever")
      expect(@callback_called).to be_falsey
      @container.log(:error, "Some info about whatever")
      expect(@callback_called).to be_truthy
    end

  end

  describe "syncing adapters" do

    before(:each) do
      @currency_layer_adapter_rates = { "RUB" => BigDecimal("68.0") }
      @container = CurrencyRate::Container.new(adapter_names: ["Bitstamp", "CurrencyLayer"])
      allow(@container.storage).to receive(:write)
      expect(@container.adapters[1]).to receive(:fetch_rates).and_return(@currency_layer_adapter_rates)
    end

    it "logs the warning that we didn't get the rates from the adapter's source" do
      expect(@container.adapters[0]).to receive(:fetch_rates).and_return(nil)
      expect(@container.storage).to receive(:write).with("currency_layer", @currency_layer_adapter_rates)
      expect(@container).to receive(:log).with(:warn, /^Trying to sync rates for BitstampAdapter/)
      @container.sync!
    end

    it "rescues a StandardError (usually has to do with connection issues) and logs a warning" do
      expect(@container.adapters[0]).to receive(:fetch_rates).and_raise("Some error")
      expect(@container).to receive(:log).with(:error, kind_of(StandardError))
      @container.sync!
    end

    it "returns successful and failed adapter names" do
      expect(@container.adapters[0]).to receive(:fetch_rates).and_return(nil)
      expect(@container).to receive(:log).with(:warn, /^Trying to sync rates for BitstampAdapter/)
      expect(@container.sync!).to match_array([["CurrencyLayerAdapter"], ["BitstampAdapter"]])
    end

  end

  describe "getting rates for a particular pair" do

    before(:each) do
      @container = CurrencyRate::Container.new(adapter_names: ["Bitstamp", "CurrencyLayer"])
    end

    it "logs a WARN message for each adapter where this pair was not found" do
      expect(@container.adapters[0]).to receive(:get_rate).and_return(nil)
      expect(@container.adapters[1]).to receive(:get_rate).and_return(BigDecimal("68.0"))
      expect(@container).to receive(:log).with(:warn, 'No rate for pair USD/RUB found in Bitstamp adapter')
      @container.get_rate("USD", "RUB")
    end

    it "logs an ERROR message when a pair is not found in any of the adapters" do
      expect(@container.adapters[0]).to receive(:get_rate).and_return(nil)
      expect(@container.adapters[1]).to receive(:get_rate).and_return(nil)
      expect(@container).to receive(:log).with(:error, 'No rate for pair USD/RUB is found in any of the available adapters (Bitstamp, CurrencyLayer)')
      @container.get_rate("USD", "RUB")
    end

    it "returns rate from a specified adapter" do
      expect(@container.adapters[1]).to receive(:get_rate).and_return(BigDecimal("68.0"))
      @container.get_rate("USD", "RUB", %w(CurrencyLayer))
    end

    it "returns average rate from all specified and available adapters" do
      expect(@container.adapters[0]).to receive(:get_rate).and_return(BigDecimal("68.0"))
      expect(@container.adapters[1]).to receive(:get_rate).and_return(BigDecimal("67.0"))
      expect(@container.get_rate("USD", "RUB")).to eq(BigDecimal("67.5"))
    end

    it "returns majority-average rate based on all specified and available adapters" do
      container = CurrencyRate::Container.new(adapter_names: ["Bitstamp", "Fixer", "CurrencyLayer"])
      expect(container.adapters[0]).to receive(:get_rate).and_return(BigDecimal("68.0"))
      expect(container.adapters[1]).to receive(:get_rate).and_return(BigDecimal("50.0"))
      expect(container.adapters[2]).to receive(:get_rate).and_return(BigDecimal("67.0"))
      expect(container.get_rate("USD", "RUB", strategy: :majority)).to eq(BigDecimal("67.5"))
    end

  end

  describe "complementaries" do

    it "converts A into C when container adapters don't have an A/C pair, but a complementary container has an A/B pair" do
      currency_layer_container = CurrencyRate::Container.new(adapter_names: ["CurrencyLayer"])
      bitstamp_container       = CurrencyRate::Container.new(adapter_names: ["Bitstamp"], complementaries: currency_layer_container)
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).with("BTC", "USD").and_return(BigDecimal("10_000.0"))
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).with("BTC", "RUB").and_return(nil)
      expect(currency_layer_container.adapters[0]).to receive(:get_rate).with("USD", "RUB").and_return(BigDecimal("68.0"))
      expect(bitstamp_container.get_rate("BTC", "RUB").to_f).to eq(680_000)
    end

    # For instance, if COMPLEMENTARY_CURRENCIES = ["USD", "EUR", "GBP"] and none of the complementaries
    # have rates for "USD/RUB", but at least one of the complementaries has rates "EUR/RUB", it's not
    # going to check for "GBP/RUB".
    it "picks the first available complementary currency when attempting to convert rates" do
      currency_layer_container = CurrencyRate::Container.new(adapter_names: ["CurrencyLayer"])
      bitstamp_container = CurrencyRate::Container.new(
        adapter_names: ["Bitstamp"],
        complementaries: currency_layer_container,
        complementary_currencies: ["USD", "EUR", "GPB"]
      )
      expect(currency_layer_container.adapters[0]).to receive(:get_rate).with("USD", "RUB").and_return(nil)
      expect(currency_layer_container.adapters[0]).to receive(:get_rate).with("EUR", "RUB").and_return(BigDecimal('92.0'))
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).with("BTC", "RUB").and_return(nil)
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).with("BTC", "EUR").and_return(BigDecimal("9_000.0"))

      expect(bitstamp_container.get_rate("BTC", "RUB").to_f).to eq(828_000)
    end

    it "logs a warning message if no complementary currency pairs are found in any of the adapters" do
      currency_layer_container = CurrencyRate::Container.new(adapter_names: ["CurrencyLayer"])
      bitstamp_container       = CurrencyRate::Container.new(adapter_names: ["Bitstamp"], complementaries: currency_layer_container)
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).with("BTC", "RUB").and_return(nil)
      expect(currency_layer_container.adapters[0]).to receive(:get_rate).with("USD", "RUB").and_return(nil)

      expected_log_message = "Trying to convert RUB/BTC, but no rate for complementary pairUSD/RUB\n" +
                             "found in any of the complementary sources:\n" +
                             "  * Container (type: unknown) with adapters: CurrencyLayer"
      expect(bitstamp_container).to receive(:log).with(:error, /^Trying to convert/)
      expect(bitstamp_container.get_rate("BTC", "RUB")).to eq(nil)
    end

    it "uses default rate strategy (:majority or :average) of the container when getting a rate from a complementary container" do
      fiat_container = CurrencyRate::Container.new(adapter_names: ["CurrencyLayer", "FreeForex", "Fixer"])
      bitstamp_container = CurrencyRate::Container.new(adapter_names: ["Bitstamp"], complementaries: fiat_container)
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).twice.with("BTC", "USD").and_return(BigDecimal("10_000.0"))
      expect(bitstamp_container.adapters[0]).to receive(:get_rate).with("BTC", "RUB").twice.and_return(nil)
      expect(fiat_container.adapters[0]).to receive(:get_rate).with("USD", "RUB").twice.and_return(BigDecimal("68.0"))
      expect(fiat_container.adapters[1]).to receive(:get_rate).with("USD", "RUB").twice.and_return(BigDecimal("67.0"))
      expect(fiat_container.adapters[2]).to receive(:get_rate).with("USD", "RUB").twice.and_return(BigDecimal("60.0"))

      fiat_container.get_rate_strategy = :average
      expect(bitstamp_container.get_rate("BTC", "RUB").to_f).to eq(650_000)
      fiat_container.get_rate_strategy = :majority
      expect(bitstamp_container.get_rate("BTC", "RUB").to_f).to eq(675_000)
    end

  end

end
