lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "currency_rate/version"

Gem::Specification.new do |spec|
  spec.name = "currency-rate"
  spec.version = CurrencyRate::VERSION
  spec.authors = ["romanitup"]
  spec.email = "romanitup@protonmail.com"

  spec.summary = "Converter for fiat and crypto currencies"
  spec.description = "Fetches exchange rates from various sources and does the conversion"
  spec.homepage = "https://gitlab.com/hodlhodl-public/currency-rate"
  spec.require_paths = ["lib"]
end

