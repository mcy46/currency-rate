module CurrencyRate
  class FileStorage
    attr_reader   :path
    attr_accessor :serializer

    def initialize(path:, container:, serializer: nil)
      @path       = path
      @container  = container
      @serializer = (serializer || Storage::YAMLSerializer).new
    end

    def exists?(adapter_name)
      File.exists? path_for(adapter_name)
    end

    def read(adapter_name)
      path = path_for adapter_name
      @serializer.deserialize File.read(path)
    rescue StandardError => e
      @container.log(:error, e)
      nil
    end

    def write(adapter_name, data = "")
      File.write path_for(adapter_name), @serializer.serialize(data)
    rescue StandardError => e
      @container.log(:error, e)
    end

    def path_for(adapter_name)
      File.join @path, "#{adapter_name}.yml"
    end
  end
end
