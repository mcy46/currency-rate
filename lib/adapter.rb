module CurrencyRate
  class Adapter
    include Singleton

    # This constant is used in CurrencyRate::Container#get_rates_with_complemenataries
    # when, for example, we're trying to convert BTC into RUB, but container's get_rates cannot
    # find the pair BTC/RUB in any of its adapters, but it has BTC/USD. We can then make use of
    # this and try to calculate the rate using a different container and/or adapter
    # that has RUB/USD rate.
    COMPLEMENTARY_CURRENCIES = ["USD"]

    SUPPORTED_CURRENCIES = []
    FETCH_URL = nil

    attr_reader   :rates
    attr_accessor :container
    attr_writer   :complementary_currencies
    attr_accessor :api_key

    def complementary_currencies
      @complementary_currencies || self.class::COMPLEMENTARY_CURRENCIES
    end

    def name(format=:snake_case)
      @camel_case_name ||= self.class.name.gsub(/^.*::/, "").sub("Adapter", "")
      @snake_case_name ||= @camel_case_name.to_snake_case
      format == :camel_case ? @camel_case_name : @snake_case_name
    end

    def get_rate(from, to)
      @rates = @container.storage.read(self.name) if @rates.nil? && @container.storage.exists?(self.name)
      return BigDecimal(rates[to])                if self.class::ANCHOR_CURRENCY == from && rates[to]
      return BigDecimal(1 / rates[from])          if self.class::ANCHOR_CURRENCY == to && rates[from]
      return BigDecimal(rates[to] / rates[from])  if rates[from] && rates[to]
      nil
    end

    def fetch_rates
      begin
        @rates = normalize(exchange_data)
      rescue StandardError => e
        @container.log(:error, e)
        nil
      end
    end

    def normalize(data)
      @rates = if data.nil?
        @container.log(:warn, "#{self.name}#normalize: data is nil")
        return nil
      else
        parse_raw_data(data)
      end
    end

    def exchange_data
      raise "FETCH_URL is not defined!" unless self.class::FETCH_URL

      begin
        if self.class::FETCH_URL.kind_of?(Hash)
          self.class::FETCH_URL.each_with_object({}) do |(name, url), result|
            result[name] = request url
          end
        else
          result = request self.class::FETCH_URL
        end
      rescue StandardError => e
        @container.log(:error, e)
        nil
      end
    end

    def url_with_api_key(url)
      if url.include?("__API_KEY__")
        if api_key.nil?
          @container.log(:error, "API key for #{self.name} is not set")
          return nil
        else
          url.sub("__API_KEY__", api_key.strip)
        end
      else
        url
      end
    end

    def request(url)
      unless url = url_with_api_key(url)
        return nil
      end
      http_client = HTTP.timeout(connect: @container.connect_timeout, read: @container.read_timeout)
      http_client
        .headers("Accept" => "application/json; version=1")
        .headers("Content-Type" => "text/plain")
        .get(url)
        .to_s
    end

    def parse_raw_data(data)
      if data.kind_of?(Hash)
        data.each { |k,v| data[k] = parse_raw_data(v) }
        data
      else
        begin
          return JSON.parse(data)
        rescue JSON::ParserError
          return data
        end
      end
    end

  end
end
