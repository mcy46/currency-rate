module CurrencyRate
  class CoinmonitorAdapter < Adapter
    # No need to use it for fetching, just additional information about supported currencies
    SUPPORTED_CURRENCIES = %w(ARS)
    ANCHOR_CURRENCY = "USD"
    FETCH_URL = "https://ar.coinmonitor.info/data_ar.json"

    def normalize(data)
      return nil unless data = super
      { "anchor" => ANCHOR_CURRENCY }.merge({ SUPPORTED_CURRENCIES.first => BigDecimal(data["BTC_avr_ars"].to_s) })
    end
  end
end
