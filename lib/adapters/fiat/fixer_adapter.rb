module CurrencyRate
  class FixerAdapter < Adapter
    # EUR is the only currency available as a base on free plan
    ANCHOR_CURRENCY = "EUR"
    FETCH_URL = "http://data.fixer.io/api/latest?access_key=__API_KEY__&base=#{ANCHOR_CURRENCY}"

    def normalize(data)
      return nil unless data = super
      rates = { "anchor" => ANCHOR_CURRENCY }
      data["rates"].each do |k,v|
        rates[k] = BigDecimal(v.to_s)
      end
      rates
    end

  end
end
