module CurrencyRate
  class OkcoinAdapter < Adapter
    FETCH_URL = {
      'LTC_USD' => 'https://www.okcoin.com/api/spot/v3/instruments/LTC-USD/ticker',
      'BTC_USD' => 'https://www.okcoin.com/api/spot/v3/instruments/BTC-USD/ticker',
    }

    def normalize(data)
      return nil unless data = super
      data.reduce({}) do |result, (pair, value)|
        result[pair] = BigDecimal(value["last"].to_s)
        result
      end
    end

  end
end
