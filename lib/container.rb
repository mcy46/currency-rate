class CurrencyRate::Container

  attr_accessor :api_keys
  attr_accessor :logger
  attr_accessor :logger_on
  attr_accessor :adapters
  attr_accessor :connect_timeout
  attr_accessor :read_timeout
  attr_accessor :storage
  attr_accessor :logger_callbacks
  attr_accessor :limit_adapters_for_pairs
  attr_accessor :get_rate_strategy

  def initialize(
    adapter_type: nil,
    adapter_names: nil,
    get_rate_strategy: :average,
    complementaries: nil,
    complementary_currencies: nil,
    api_keys: CurrencyRate.default_config[:api_keys],
    limit_adapters_for_pairs: CurrencyRate.default_config[:limit_adapters_for_pairs],
    storage_settings: CurrencyRate.default_config[:storage_settings],
    connect_timeout: CurrencyRate.default_config[:connect_timeout],
    read_timeout: CurrencyRate.default_config[:read_timeout],
    logger_settings: CurrencyRate.default_config[:logger_settings],
    logger_callbacks: CurrencyRate.default_config[:logger_callbacks]
  )

    logger_settigns = CurrencyRate.default_config[:logger_settings].merge(logger_settings)
    storage_settigns = CurrencyRate.default_config[:storage_settings].merge(storage_settings)

    method(__method__).parameters.map do |_, name|
      value = binding.local_variable_get(name)
      instance_variable_set("@#{name}", value)
    end

    _logger_callbacks = {}
    @logger_callbacks.each do |k,v|
      _logger_callbacks[Logger::Severity.const_get(k.to_s.upcase)] = v
    end
    @logger_callbacks = _logger_callbacks

    _limit_adapters_for_pairs = {}
    @limit_adapters_for_pairs.each do |pair,adapters|
      adapters = [adapters] unless adapters.kind_of?(Array)
      adapters.each do |a|
        _limit_adapters_for_pairs[pair] = CurrencyRate.const_get(a.to_s.to_camel_case + "Adapter").instance
      end
    end
    @limit_adapters_for_pairs = _limit_adapters_for_pairs

    @storage = CurrencyRate.const_get(storage_settigns[:type].to_s.to_camel_case + "Storage").new(
      path: storage_settings[:path],
      container: self,
      serializer: storage_settings[:serializer]
    )

    load_adapters!(names: adapter_names, type: adapter_type)
  end

  # Returns adapter names as a String, separated by commas.
  # This is useful in get_rates_with_complemenataries when loggin failures.
  def name
    "Container (type: #{@type || "unknown"}) with adapters: #{@adapter_names.join(", ")}"
  end

  def logger
    return @logger if @logger
    @logger = Logger.new(@logger_settings[:device])
    @logger.progname  = "CurrencyRate"
    @logger.level     = @logger_settings[:level]
    @logger.formatter = @logger_settings[:formatter] if @logger_settings[:formatter]
    @logger
  end

  # This method doesn't make any requests. It merely reads normalized data 
  # from the selected storage. Contrary to what one might assume,
  # it doesn't call CurrencyRate::Container.sync! if storage for a particular
  # adapter doesn't exist - that's because those are two separate tasks
  # and you might not want to make that external http(s) request even if
  # the rates in storage are not found.
  #
  # It also uses a 3 different strategies to calculate rates before they are returned:
  #
  # 1. If only one adapter is specified and/or available, it will return the rate for the
  #    pair using the data from the storage for this particular adapter.
  #
  # 2. If two or more adapters are specified and available, it will return the average rate
  #    based on the the rates from all of those adapters.
  #
  # 3. If `strategy: :majority` flag is set and an odd number of adapters (say, 3) 
  #    is passed and/or available it will look at the rates from all, separate them
  #    into two groups based on how close their rates are and return the average of the rates
  #    that are closest in the group that is the largest.
  #
  # It will also check @limit_adapters_for_pairs hash for the request pair
  # and if the key exists it will exclude adapters that are not listed in the array
  # under that key.
  def get_rate(from, to, use_adapters=@adapter_names, strategy: @get_rate_strategy, complementaries: @complementaries)
    user_adapters = [use_adapters] if user_adapters.kind_of?(String)
    adapters_for_pair = @limit_adapters_for_pairs["#{from}/#{to}"]
    _adapters = adapters.select  { |a| use_adapters.include?(a.name(:camel_case)) }
    if adapters_for_pair
      _adapters = _adapters.select { |a| adapters_for_pair.include?(a.name(:camel_case)) }
    else
      _adapters = _adapters.reject { |a| @limit_adapters_for_pairs.include?(a.name(:camel_case)) }
    end

    rates, loggers = if complementaries
      get_rates_with_complementaries(from, to, _adapters, complementaries)
    else
      get_rates_from_adapters(from, to, _adapters)
    end
    rates.reject! { |r| r[1].nil? }

    loggers.each { |wl| wl.call }

    if rates.size == 1
      rates[0][1]
    elsif rates.empty?
      nil
    else
      rates = apply_majority_strategy_to(rates) if strategy == :majority && rates.size.odd?
      apply_average_strategy_to(rates)
    end

  end

  def sync!
    successfull = []
    failed      = []
    @adapters.each do |adapter|
      adapter_name = adapter.class.to_s.sub("CurrencyRate::", "")
      begin
        rates = adapter.fetch_rates
        unless rates
          self.log(:warn, "Trying to sync rates for #{adapter_name}, rates not found, but http(s) request did not return any error.")
          failed << adapter_name
          next
        end
        @storage.write(adapter_name.to_snake_case.sub("_adapter", ""), rates)
        successfull << adapter_name
      rescue StandardError => e
        failed << adapter_name
        self.log(:error, e)
        next
      end
    end
    [successfull, failed]
  end

  def log(level, message)
    severity = Logger::Severity.const_get(level.to_s.upcase)
    logger.send(level, message)
    if @logger_callbacks[severity]
      @logger_callbacks[severity].call(level, message)
    else
      @logger_callbacks.keys.sort.each do |k|
        @logger_callbacks[k].call(level, message) && break if k < severity
      end
    end
  end

  private

  def apply_majority_strategy_to(rates)
    rates.sort! { |r1, r2| r1[1] <=> r2[1] }

    largest_discrepancy_rate_index = 0
    last_discrepancy = 0

    rates.each_with_index do |r,i|
      if i > 0
        discrepancy = r[1] - rates[i-1][1]
        if discrepancy > last_discrepancy
          last_discrepancy = discrepancy
          largest_discrepancy_rate_index = i
        end
      end
    end

    rates_group_1 = rates[0...largest_discrepancy_rate_index]
    rates_group_2 = rates[largest_discrepancy_rate_index..rates.size-1]
    [rates_group_1, rates_group_2].max { |g1,g2| g1.size <=> g2.size }
  end

  def apply_average_strategy_to(rates)
    rates.inject(BigDecimal(0)) { |sum, i| sum + i[1] } / BigDecimal(rates.size)
  end

  def get_rates_from_adapters(from, to, _adapters, logger_on: true)
    loggers = []
    rates = _adapters.map do |a|
      rate = [a.name(:camel_case), a.get_rate(from, to)]
      if logger_on
        loggers.push -> {
          self.log(:warn, "No rate for pair #{from}/#{to} found in #{a.name(:camel_case)} adapter") if rate[1].nil?
        }
      end
      rate
    end
    if rates.reject { |r| r[1].nil? }.empty?
      loggers = [ -> {
        self.log(:error,
          "No rate for pair #{from}/#{to} is found in any of the available adapters " +
          "(#{_adapters.map { |a| a.name(:camel_case) }.join(", ")})"
        )
      }]
    end
    [rates, loggers]
  end

  def get_rates_with_complementaries(from, to, _adapters, complementaries)
    complementaries = [complementaries] unless complementaries.kind_of?(Array)
    rates, _l = get_rates_from_adapters(from, to, _adapters, logger_on: false)
    loggers = []
    rates.map! do |r|
      if r[1].nil?

        complementary_rates = []
        complementary_currency_used = nil
        complementary_currencies = @complementary_currencies || @adapters_by_name[r[0]].complementary_currencies
        complementary_currencies.each do |cc|
          complementary_currency_used = cc
          complementary_rates = complementaries.map do |c|
            [c.name, c.get_rate(cc, to)]
          end
          break unless complementary_rates.reject { |r| r[1].nil? }.empty?
        end

        failed_complementary_rates = complementary_rates.select { |r| r[1].nil? }
        valid_complementary_rates  = complementary_rates.reject { |r| r[1].nil? }
        if failed_complementary_rates.size == complementary_rates.size
          loggers.push -> {
            message = "Trying to convert #{to}/#{from}, but no rate for complementary pair" +
                      "#{@adapters_by_name[r[0]].complementary_currencies.join(",")}/#{to}\n" +
                      "found in any of the complementary sources:\n" +
                      "  * #{complementaries.map(&:name).join("\n  *")}"
            self.log(:error, message)
          }
          r
        elsif !valid_complementary_rates.empty?
          new_from = apply_average_strategy_to(valid_complementary_rates).to_f.to_s
          new_to = get_rate(from, complementary_currency_used).to_f.to_s
          [r[0], BigDecimal(new_from)*BigDecimal(new_to)]
        end
      else
        r
      end
    end.compact
    complementaries.each { |c| c.logger_on = true if c.kind_of?(CurrencyRate::Container) }
    [rates, loggers]
  end

  def load_adapters!(names: nil, type: :all)
    if names
      names = [names] if names.kind_of?(String)
      @adapters = names.map do |name|
        CurrencyRate.const_get(name + "Adapter").instance
      end
    else
      crypto_adapter_files = Dir[File.join CurrencyRate.root, "lib/adapters/crypto/*"]
      fiat_adapter_files   = Dir[File.join CurrencyRate.root, "lib/adapters/fiat/*"]
      adapter_files = case type
        when :crypto  then crypto_adapter_files
        when :fiat    then fiat_adapter_files
        else               crypto_adapter_files + fiat_adapter_files
      end
      @adapters = adapter_files.map do |file|
        CurrencyRate.const_get(File.basename(file, ".rb").to_camel_case).instance
      end
    end
    @adapter_names    = []
    @adapters_by_name = {}
    @adapters.each do |a|
      a.container = self
      a.api_key = @api_keys[a.name]
      @adapter_names << a.name(:camel_case)
      @adapters_by_name[a.name(:camel_case)] = a
    end
    @adapters
  end


end
