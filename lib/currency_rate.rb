require "bigdecimal"
require "logger"
require "singleton"
require "json"
require "http"

require_relative "exceptions"
require_relative "adapter"
require_relative "container"
require_relative "utils/string_extensions"

Dir["#{File.expand_path __dir__}/adapters/**/*.rb"].each { |f| require f }
Dir["#{File.expand_path __dir__}/storage/**/*.rb"].each  { |f| require f }

module CurrencyRate

  @@default_config = {
    connect_timeout: 10,
    read_timeout: 10,
    api_keys: {},
    limit_adapters_for_pairs: {},
    logger_callbacks: {},
    logger_settings:  { device: $stdout, level: :info, formatter: nil },
    storage_settings: { path: __dir__, type: :file, serializer: CurrencyRate::Storage::YAMLSerializer }
  }

  class << self

    def default_config
      @@default_config
    end

    def update_default_config(config_hash)
      return unless config_hash
      config_hash.each do |k,v|
        if v.kind_of?(Hash)
          @@default_config[k] = @@default_config[k].merge(v)
        else
          @@default_config[k] = v
        end
      end
    end

    def root
      File.expand_path("../", File.dirname(__FILE__))
    end

  end

end
