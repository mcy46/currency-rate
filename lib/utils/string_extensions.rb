module StringExtensions

  def to_snake_case
    self.to_s.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end

  def to_camel_case
    words = self.to_s.split('_')
    words.map { |w| w[0].capitalize + w[1..-1] }.join
  end

end

class String; include StringExtensions; end
class Symbol; include StringExtensions; end
